
#include <Servo.h> 

Servo myservo;

//define and declare variables for servo motor
const int servo_pin = 2;

//below declare ultrasonic sensor variables 
//on the arduino you connect to these pins  
const int trig_pin = 3;
const int echo_pin = 4;
//inter time is used to calculate time to wait before closing bin
const int inter_time = 200;

int time = 0;

void setup() 
{
  Serial.begin(9600);
  myservo.attach(servo_pin, 500, 2400);
  myservo.write(90);
  pinMode (trig_pin, OUTPUT);
  pinMode (echo_pin, INPUT);
  delay(3000);
} 

//this code loops to listen to ultrasonic sensor and tell sevo motor to open up the bin
void loop() 
{
  float duration, distance;
  digitalWrite(trig_pin, HIGH);
  delayMicroseconds(1000);
  digitalWrite(trig_pin, LOW);
  duration = pulseIn (echo_pin, HIGH);
  // Distance is half the duration divided by 29.1 (from datasheet) 
  distance = (duration/2)/29;
  Serial.print(distance);
  Serial.println(" cm");
  time = time + inter_time;
  delay(inter_time);
  // if distance less than 0.1 meter(10cm) and more than 0 (0 or less means over range) 
  if (distance < 10)
  {
    //Then open the bin by moving the servo motor for 180 degrees
    for(int i = 2000; i >= 1100; i-=25){
      myservo.writeMicroseconds(i);
      Serial.println("Dustbin Opening");
      delay(100);
    }
    //This code here waits for 1000 milliseconds then closes back the lid by rotating servo motor for 180 degrees counter-clockwise
    delay(1000);
    for(int i = 1100; i <= 2000; i+=25){
      myservo.writeMicroseconds(i);
      Serial.println("Dustbin Closing");
      delay(100);
    }
  }
}
