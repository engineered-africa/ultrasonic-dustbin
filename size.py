import os
import subprocess

# Fetch all LFS files
subprocess.run(["git", "lfs", "fetch", "--all"], check=True)

# Get a list of all LFS-tracked files with full OIDs
result = subprocess.run(["git", "lfs", "ls-files", "--long"], capture_output=True, text=True, check=True)
lfs_files = result.stdout.splitlines()

# Parse the LFS file list and get their sizes
sizes = []
for line in lfs_files:
    parts = line.split()
    oid = parts[0]  # The first part is the full OID
    path = ' '.join(parts[2:])  # The path starts from the third part onward
    object_path = os.path.join(".git/lfs/objects", oid[:2], oid[2:4], oid)
    if os.path.isfile(object_path):
        size = os.path.getsize(object_path)
        sizes.append((oid, path, size))

# Sort the sizes list from largest to smallest
sizes.sort(key=lambda x: x[2], reverse=True)

# Print the sorted sizes
for oid, path, size in sizes:
    print(f"{path} ({oid}): {size} bytes")
